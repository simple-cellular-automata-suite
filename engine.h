#ifndef _ENGINE_H_
#define _ENGINE_H_

#include <stdio.h>

/* This function is intended to be an interface that all engines
 * implement, so that a programmer (you) can implement a ruleset for a
 * CA-like system by simply implementing these functions, then linking
 * the result against a driver (e.g. the sample driver defined by
 * driver.c). These functions SHALL return 0 for success, a nonzero
 * value for failure. If this value is passed to con_errstring it
 * SHOULD result in a useful error string.
 *
 * The engine can expect a driver flow of:
 *
 * - exactly one call of con_initialize_f | con_initialize_wh
 *
 * - any calls to any functions except con_initialize_f,
 *   con_initialize_wh, con_teardown
 *
 * - exactly one call of con_teardown
 *
 * Any other flow
 */

/* Initialization routines. Read in from a file (which should be some
 * kind of meaningful encoding, perhaps particular to the engine) or
 * simply initialize empty[*] on a rectangular grid.
 *
 * * ``Why do you close your eyes?'' Sussman asked his teacher.  ``So
 *   that the room will be empty.'' replied Minsky.
 */
int con_initialize_f(FILE * ifp);
int con_initialize_wh(int width, int height);

/* Get the dimensions of the game. These should return consistent
 * values once an initialization has occured
 */
int con_width(void);
int con_height(void);

/* Take another step in the simulation */
int con_step(void);

/* Set *rep such that *rep is the null-terminated string for
 * display . It is the engine's responsibility to handle allocation
 * and cleanup for the internal representation returned. Any con_ call
 * invalidates the data returned by con_representation.
 *
 * Note: The out parameter is a char *, not a char, so that engines
 * can return smiley faces and dromedary camels and all that jazz.
 */
int con_rep_at(int x, int y, const char ** const rep);

/* Set *n, *titles such that *titles is an array of *n null-terminated
 * strings representing the names of different values that would need
 * to be provided in order to re-define an arbitrary cell.
 *
 * Standard Conway's Game of Life rules might, for example, set *n to
 * 1 and *titles to { "Alive?" }. This presentation form is for
 * engines that internally represent more complicated objects.
 *
 * This method SHALL set *n and *titles consistantly once an
 * initialization method is called
 */
int con_input_titles(int * const n, const char * const ** const titles);

/* Given an array of null-terminated strings (where it is the driver's
 * responsibility to ensure that the length of vals is equal to the
 * value which *n is set to in con_input_titles), set the cell at x, y
 * to represent those values
 */
int con_set_val(int x, int y, const char ** vals);

/* Clean up all internal resources. Any con_ call after con_teardown
 * is undefined.
 */
int con_teardown(void);

/* If a suitably complex error detection system is in place in the
 * engine, this function SHOULD be implemented to return a meaningful
 * error message for any possible return value of any of the above
 * functions.
 */
const char *con_errstring(int errno);

#endif
