CC=gcc
LD=gcc
CFLAGS=-g -Wall -Werror -pedantic -ansi -O2 $(shell pkg-config --cflags ncurses)
LDFLAGS=$(shell pkg-config --libs ncurses) -lm

PROGS=game-of-sinners game-of-conway

default: all

clean:
	find -name '*.o' -delete
	find -name '*~' -delete
	rm -f $(PROGS)

all: $(PROGS)

game-of-conway: driver.o engine-conway.o
	$(LD) $(LDFLAGS) -o $@ $^

game-of-sinners: driver.o engine-sinners.o
	$(LD) $(LDFLAGS) -o $@ $^

driver.o: driver.c engine.h
	$(CC) $(CFLAGS) -o $@ -c $<

engine-conway.o: engine-conway.c engine.h
	$(CC) $(CFLAGS) -o $@ -c $<

engine-sinners.o: engine-sinners.c engine.h
	$(CC) $(CFLAGS) -o $@ -c $<
