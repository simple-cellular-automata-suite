/* A pretty dumb implementation of Conway's rules.  Really only used
 * for testing the driver
 */

#include <string.h>
#include <stdlib.h>

#include "engine.h"

#define E_UNIMPLEMENTED       -1
#define E_ILLEGAL_COORDINATES -2
#define E_UNINITIALIZED       -3
#define E_NULLREP             -4
#define E_MALFORMED_INPUT     -5

static const char *ALIVE = ".";
static const char *DEAD = " ";

static char *alive_stat = NULL;
static char *nigh_count = NULL;
static char *freeze_alive_stat = NULL;
static char *freeze_nigh_count = NULL;
static int g_width, g_height;

static void n_add(int x, int y, int mod)
{
  if (x - 1 >= 0) {
    if (y - 1 >= 0)       nigh_count[g_width * (y - 1) + (x - 1)] += mod;
                          nigh_count[g_width * (y    ) + (x - 1)] += mod;
    if (y + 1 < g_height) nigh_count[g_width * (y + 1) + (x - 1)] += mod;
  }

  if (y - 1 >= 0)         nigh_count[g_width * (y - 1) + (x    )] += mod;
  if (y + 1 < g_height)   nigh_count[g_width * (y + 1) + (x    )] += mod;

  if (x + 1 < g_width) {
    if (y - 1 >= 0)       nigh_count[g_width * (y - 1) + (x + 1)] += mod;
                          nigh_count[g_width * (y    ) + (x + 1)] += mod;
    if (y + 1 < g_height) nigh_count[g_width * (y + 1) + (x + 1)] += mod;
  }

}

int con_initialize_f(FILE * ifp){
  int i_width, i_height;
  int c = EOF;
  int i, j;
  size_t m = 0;

  if (fscanf(ifp, " %d x %d ", &i_width, &i_height) != 2)
    return E_MALFORMED_INPUT;

  if (i_width <= 0 || i_height <= 0)
    return E_MALFORMED_INPUT;

  g_width = i_width;
  g_height = i_height;
  alive_stat = calloc(g_width * g_height, sizeof(*alive_stat));
  nigh_count = calloc(g_width * g_height, sizeof(*nigh_count));
  freeze_alive_stat = malloc(g_width * g_height * sizeof(*alive_stat));
  freeze_nigh_count = malloc(g_width * g_height * sizeof(*nigh_count));


  for (j = 0; j < g_height; ++j) {
    for (i = 0; i < g_width; ++i) {
      do {
        c = fgetc(ifp);
      } while (c != ' ' && c != '.' && c != EOF);

      if (c == EOF)
        return E_MALFORMED_INPUT;
      else if (c == '.') {
        alive_stat[m] = 1;
        n_add(i, j, 1);
      }
      m++;
    }
  }

  return 0;
}

int con_initialize_wh(int width, int height)
{
  if (width <= 0 || height <= 0)
    return E_MALFORMED_INPUT;

  alive_stat = calloc(width * height, sizeof(*alive_stat));
  nigh_count = calloc(width * height, sizeof(*nigh_count));
  freeze_alive_stat = malloc(width * height * sizeof(*alive_stat));
  freeze_nigh_count = malloc(width * height * sizeof(*nigh_count));
  g_width = width;
  g_height = height;

  return 0;
}

static const char *title = "Alive? [0/1]";

int con_input_titles(int * const n, const char * const ** const titles)
{
  *n = 1;
  *titles = &title;
  return 0;
}

int con_set_val(int x, int y, const char ** vals)
{
  char v;

  if (!alive_stat)
    return E_UNINITIALIZED;

  if (x < 0 || y < 0 || x >= g_width || y >= g_height || !vals || !vals[0])
    return E_ILLEGAL_COORDINATES;

  v = (vals[0][0] == '1' || vals[0][0] == '.');

  if (!v && alive_stat[y * g_width + x]) {
    n_add(x, y, -1);
    alive_stat[y * g_width + x] = 0;
  } else if (v && !alive_stat[y * g_width + x]) {
    n_add(x, y, 1);
    alive_stat[y * g_width + x] = 1;
  }

  return 0;
}
int con_set_p(int x, int y, void *val){ return E_UNIMPLEMENTED; }

int con_width(void)
{
  if (!alive_stat)
    return E_UNINITIALIZED;

  return g_width;
}

int con_height(void)
{
  if (!alive_stat)
    return E_UNINITIALIZED;

  return g_height;
}

int con_step(void)
{
  int x, y;
  size_t m = 0;

  if (!alive_stat)
    return E_UNINITIALIZED;

  memcpy(freeze_alive_stat, alive_stat,
         g_width * g_height * sizeof (*alive_stat));
  memcpy(freeze_nigh_count, nigh_count,
         g_width * g_height * sizeof (*nigh_count));

  for (y = 0; y < g_height; ++y) {
    for (x = 0; x < g_width; ++x) {
      if (freeze_alive_stat[m]
          && (freeze_nigh_count[m] < 2 || freeze_nigh_count[m] > 3)) {
        alive_stat[m] = 0;
        n_add(x, y, -1);
      } else if (!freeze_alive_stat[m] && freeze_nigh_count[m] == 3) {
        alive_stat[m] = 1;
        n_add(x, y, 1);
      }
      ++m;
    }
  }
  return 0;
}
int con_rep_at(int x, int y, const char ** const rep)
{
  if (!alive_stat)
    return E_UNINITIALIZED;

  if (!rep)
    return E_NULLREP;

  if (x < 0 || y < 0 || x >= g_width || y >= g_height)
    return E_ILLEGAL_COORDINATES;

  if (alive_stat[y * g_width + x])
    *rep = ALIVE;
  else
    *rep = DEAD;

  return 0;
}

int con_teardown(void)
{
  if (alive_stat)
    free(alive_stat);
  if (nigh_count)
    free(nigh_count);
  if (freeze_alive_stat)
    free(freeze_alive_stat);
  if (freeze_nigh_count)
    free(freeze_nigh_count);

  return 0;
}

const char *con_errstring(int errno)
{
  switch (errno) {
  case 0:                     return "No error";
  case E_UNIMPLEMENTED:       return "Unimplemented function";
  case E_UNINITIALIZED:       return "Uninitialized";
  case E_ILLEGAL_COORDINATES: return "Illegal coordinates";
  case E_NULLREP:             return "Null pointer passed";
  case E_MALFORMED_INPUT:     return "Malformed input file";
  default:                    return "Unknown error";
  }
}
