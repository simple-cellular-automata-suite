#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "engine.h"

#define FPS_CAP 10
#define E_BAD_FILE 1
#define E_BAD_SCREEN_DIMS 2
#define E_ENGINE_INITIALIZE 3
#define E_ENGINE_ERROR 4
#define E_USAGE 9

static int err;

int inp_cell(int height, int width, int s_height, int s_width, int yoff,
             int xoff)
{
  int i, j;
  static int x = -1, y = -1;
  int c;

  int n = 0;
  const char * const *titles = NULL;
  char **values;

  if (x < 1)
    x =  width / 2;
  if (y < 1)
    y =  height / 2;

  if (con_input_titles(&n, &titles))
    return 1;

  values = calloc(n, sizeof(*values));

  move(s_height, 0);
  for (j = 0; j < s_width; ++j)
    addch(' ');

  mvaddstr(s_height, 0, "Select:");
  refresh();

  curs_set(1);

  do {

    if (x < 0)
      x = 0;
    else if (x >= width)
      x = width - 1;
    if (y < 0)
      y = 0;
    else if (y >= height)
      y = height - 1;

    move(yoff + y, xoff + x);

    c = getch();
    switch(c) {
    case 'h': case KEY_LEFT:  x--;        break;
    case 'l': case KEY_RIGHT: x++;        break;
    case 'j': case KEY_DOWN:        y++;  break;
    case 'k': case KEY_UP:          y--;  break;
    case 'y': case KEY_A1:    x--;  y--;  break;
    case 'u': case KEY_A3:    x++;  y--;  break;
    case 'b': case KEY_C1:    x--;  y++;  break;
    case 'n': case KEY_C3:    x++;  y++;  break;
    case 'H':                 x-=8;       break;
    case 'L':                 x+=8;       break;
    case 'J':                       y+=8; break;
    case 'K':                       y-=8; break;
    case 'Y':                 x-=8; y-=8; break;
    case 'U':                 x+=8; y-=8; break;
    case 'B':                 x-=8; y+=8; break;
    case 'N':                 x+=8; y+=8; break;

    }
  } while(c != ' ' && c != '\n' && c != '\r' && c != KEY_ENTER);

  for (i = 0; i < n; ++i) {
    values[i] = calloc(128, sizeof(*values[i]));
    move(s_height, 0);
    for (j = 0; j < s_width; ++j)
      addch(' ');
    mvaddstr(s_height, 0, titles[i]);
    addstr(" ");
    refresh();
    echo();
    nocbreak();
    timeout(-1);
    getstr(values[i]);
    noecho();
    cbreak();
    timeout(1000 / FPS_CAP);
  }

  err = con_set_val(x, y, (const char **)values);

  for (i = 0; i < n; ++i)
    free(values[n]);
  free(values);

  curs_set(0);
  return !err;
}

void bottom_bar(int height, int width, char run)
{
  int i;

  move(height, 0);
  for (i = 0; i < width; ++i)
    addch(' ');
  move(height, 0);

  addstr("[ ");
  if (run) {
    addstr("Running (");
    addch('P' | A_STANDOUT);
    addstr("ause)     ] ");
  } else {
    addstr("Paused  (");
    addch('R' | A_STANDOUT);
    addstr("un, ");
    addch('S' | A_STANDOUT);
    addstr("tep) ] ");
  }
  addstr("[ Set ");
  addch('V' | A_STANDOUT);
  addstr("alue ] [");
  addch('Q' | A_STANDOUT);
  addstr("uit ]");
  refresh();
}

int displayloop(void)
{
  int s_width = -1, s_height = -1, o_width = -1, o_height = -1;
  int sx = -1, sy = -1;
  int c = 0, i, j;
  int g_width = con_width(), g_height = con_height();
  char run = 0, should_quit = 0, onestep = 0, skip_this_calc = 0;

  const char *cell = NULL;

  do {
    onestep = 0;
    getmaxyx(stdscr, s_height, s_width);
    s_height--;

    if (s_height != o_height || s_width != o_width) {
      clear();
      refresh();
      o_height = s_height;
      o_width = s_width;

      sy = (s_height - g_height - 1) / 2;
      sx = (s_width - g_width - 1) / 2;

      if (sy < 0 || sx < 0)
        return E_BAD_SCREEN_DIMS;

      if (sy > 0) {
        if (sx > 0)
          mvaddch((sy - 1), (sx - 1), ACS_ULCORNER);

        move((sy - 1), sx); for (i = 0; i < g_width; ++i) addch(ACS_HLINE);

        if (sx + g_width < s_width)
          mvaddch((sy - 1), (sx + g_width), ACS_URCORNER);
      }

      if (sx > 0)
        for (i = sy; i < sy + g_height; ++i)
          mvaddch(i, (sx - 1), ACS_VLINE);

      if (sx + g_width < s_width)
        for (i = sy; i < sy + g_height; ++i)
          mvaddch(i, (sx + g_width), ACS_VLINE);

      if (sy + g_height < s_height) {
        if (sx > 0)
          mvaddch((sy + g_height), (sx - 1), ACS_LLCORNER);

        move((sy + g_height), sx);
        for (i = 0; i < g_width; ++i) addch(ACS_HLINE);

        if (sx + g_width < s_width)
          mvaddch((sy + g_height), (sx + g_width), ACS_LRCORNER);
      }
    }

    for (j = 0; j < g_height; ++j) {
      move(sy + j, sx);
      for (i = 0; i < g_width; ++i) {
        if ((err = con_rep_at(i, j, &cell)))
          break;
        addstr(cell);
      }
    }

    do {
      bottom_bar(s_height, s_width, run);

      c = getch();
      switch(c) {
      case KEY_EXIT:
      case 'Q':
      case 'q': should_quit = 1;      break;
      case 'R':
      case 'r': run = 1;              break;
      case 'P':
      case 'p': run = 0; onestep = 0; break;
      case ' ':
      case 'S':
      case 's': run = 0; onestep = 1; break;
      case 'V':
      case 'v':
        run = 0;
        onestep = 1;
        skip_this_calc = 1;
        inp_cell(g_height, g_width, s_height, s_width, sy, sx);
        break;
      case KEY_RESIZE:
        run = 0; onestep = 1; skip_this_calc = 1; break;
      }
    } while (!(run || onestep) && !should_quit && !err);

    if (should_quit || err)
      break;

    if (skip_this_calc)
      skip_this_calc = 0;
    else if ((err = con_step()))
      break;

  } while (!should_quit);

  return (!err) ? 0 : E_ENGINE_ERROR;
}

int main(int argc, const char **argv)
{
  int width = -1, height = -1, ret = 0;
  FILE *template = NULL;

  setlocale(LC_ALL, "");

  if (argc == 2) {
    if (!(template = fopen(argv[1], "r"))) {
      perror("Could not open template file");
      return E_BAD_FILE;
    }
    err = con_initialize_f(template);
    fclose(template);
  } else if (argc != 1) {
    fprintf(stderr, "Usage: %s [path-to-input-file]\n", argv[0]);
    return E_USAGE;
  }

  if (!initscr())
    return -1;
  getmaxyx(stdscr, height, width);
  cbreak();
  noecho();
  curs_set(0);
  timeout(1000 / FPS_CAP);
  clear();
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);

  if (argc == 1)
    err = con_initialize_wh(width, height - 1);

  if (err) {
    endwin();
    fprintf(stderr, "Could not initialize engine: %s\n", con_errstring(err));
    return E_ENGINE_INITIALIZE;
  }

  ret = displayloop();
  endwin();

  switch (ret) {
  case E_BAD_SCREEN_DIMS:
    fprintf(stderr, "Term dimensions too small.\n");
    break;
  case E_ENGINE_ERROR:
    fprintf(stderr, "Engine error: %s\n", con_errstring(err));
    break;
  }

  con_teardown();

  return ret;
}
