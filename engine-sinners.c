#include <stdlib.h>

#include "engine.h"
#include "math.h"

#define E_UNIMPLEMENTED       -1
#define E_ILLEGAL_COORDINATES -2
#define E_UNINITIALIZED       -3
#define E_NULLREP             -4
#define E_MALFORMED_INPUT     -5
#define E_BAD_VALUES          -6

struct cell {
  double alpha;
  double beta;
  double c_sin;
  double n_avg;
  int xi;
};

static struct cell *grid;
static size_t g_len = -1;

static long t = 0;

static int g_width, g_height;

static void compute_sins(void)
{
  size_t m;

  for (m = 0; m < g_len; ++m)
    grid[m].c_sin = sin(grid[m].beta + t * grid[m].alpha);
}

#define SET_XI(i) {                                     \
    grid[i].xi =                                        \
      floor(1.0 / fabs(grid[i].c_sin - grid[i].n_avg)); \
  }

#define ADJUST(i) {                                             \
    if (grid[i].xi % 2)                                         \
      grid[i].alpha = (asin(grid[i].n_avg) - grid[i].beta) / t; \
    else                                                        \
      grid[i].beta = asin(grid[i].n_avg) - grid[i].alpha * t;   \
  }

static void handle_xis(void)
{
  size_t i, j;

  grid[0].n_avg = (grid[1].c_sin +
                   grid[g_width].c_sin +
                   grid[g_width + 1].c_sin) / 3.0;
  SET_XI(0);
  ADJUST(0);

  grid[g_width - 1].n_avg = (grid[g_width - 2].c_sin +
                             grid[2 * g_width - 2].c_sin +
                             grid[2 * g_width - 1].c_sin) / 3.0;
  SET_XI(g_width - 1);
  ADJUST(g_width - 1);

  grid[(g_height - 1) * g_width].n_avg = (grid[(g_height - 2) * g_width].c_sin +
                                          grid[(g_height - 2) * g_width + 1].c_sin +
                                          grid[(g_height - 1) * g_width + 1].c_sin) / 3.0;
  SET_XI((g_height - 1) * g_width);
  ADJUST((g_height - 1) * g_width);

  grid[g_height * g_width - 1].n_avg = (grid[g_height * g_width - 2].c_sin +
                                        grid[(g_height - 1) * g_width - 2].c_sin +
                                        grid[(g_height - 1) * g_width - 1].c_sin) / 3.0;
  SET_XI(g_height * g_width - 1);
  ADJUST(g_height * g_width - 1);

  for (i = 1; i < g_width - 1; ++i) {
    grid[i].n_avg = (grid[i - 1].c_sin +
                     grid[i + 1].c_sin +
                     grid[g_width + i - 1].c_sin +
                     grid[g_width + i].c_sin +
                     grid[g_width + i + 1].c_sin) / 5.0;
    SET_XI(i);
    ADJUST(i);
  }

  for (i = (g_height - 1) * g_width + 1; i < g_height * g_width - 2; ++i) {
    grid[i].n_avg = (grid[i - 1].c_sin +
                     grid[i + 1].c_sin +
                     grid[i - g_width - 1].c_sin +
                     grid[i - g_width].c_sin +
                     grid[i - g_width + 1].c_sin) / 5.0;
    SET_XI(i);
    ADJUST(i);
  }

  for (j = g_width; j < g_height * g_width; j+= g_width) {
    grid[j].n_avg = (grid[j - g_width].c_sin +
                     grid[j - g_width + 1].c_sin +
                     grid[j + 1].c_sin +
                     grid[j + g_width].c_sin +
                     grid[j + g_width + 1].c_sin) / 5.0;
    SET_XI(j);
    ADJUST(j);
  }

  for (j = 2 * g_width - 1; j < g_height * g_width - 1; j+= g_width) {
    grid[j].n_avg = (grid[j - g_width].c_sin +
                     grid[j - g_width - 1].c_sin +
                     grid[j - 1].c_sin +
                     grid[j + g_width].c_sin +
                     grid[j + g_width - 1].c_sin) / 5.0;
    SET_XI(j);
    ADJUST(j);
  }

  for (j = 1; j < g_height - 1; ++j) {
    for (i = 1; i < g_width - 1; ++i) {
      grid[j * g_width + i].n_avg = (grid[(j - 1) * g_width + (i - 1)].c_sin +
                                     grid[(j - 1) * g_width + (i + 0)].c_sin +
                                     grid[(j - 1) * g_width + (i + 1)].c_sin +
                                     grid[(j + 0) * g_width + (i - 1)].c_sin +
                                     grid[(j + 0) * g_width + (i + 1)].c_sin +
                                     grid[(j + 1) * g_width + (i - 1)].c_sin +
                                     grid[(j + 1) * g_width + (i + 0)].c_sin +
                                     grid[(j + 1) * g_width + (i + 1)].c_sin) / 8.0;
      SET_XI(j * g_width + i);
      ADJUST(j * g_width + i);
    }
  }
}

int con_initialize_f(FILE * ifp)
{
  int i_width, i_height;
  int i, j, m = 0;
  double a, b;

  if (fscanf(ifp, " %d x %d ", &i_width, &i_height) != 2)
    return E_MALFORMED_INPUT;

  if (i_width <= 0 || i_height <= 0)
    return E_MALFORMED_INPUT;

  g_width = i_width;
  g_height = i_height;
  g_len = (size_t)g_width * (size_t)g_height;
  grid = calloc(g_len, sizeof(*grid));

  for (j = 0; j < g_height; ++j) {
    for (i = 0; i < g_width; ++i) {
      if (fscanf(ifp, " %lf , %lf ", &a, &b) != 2)
        return E_MALFORMED_INPUT;
      grid[m].alpha = a;
      grid[m].beta = b;
      m++;
    }
  }

  compute_sins();

  return 0;
}

int con_initialize_wh(int width, int height)
{
  if (width <= 0 || height <= 0)
    return E_MALFORMED_INPUT;

  g_width = width;
  g_height = height;
  g_len = (size_t)g_width * (size_t)g_height;
  grid = calloc(g_len, sizeof(*grid));

  return 0;
}

int con_width(void)
{
  return g_width;
}

int con_height(void)
{
  return g_height;
}

int con_step(void)
{
  ++t;
  compute_sins();
  handle_xis();

  return 0;
}

static const char *APEX = "^";
static const char *HIGH = "'";
static const char *NIL = " ";
static const char *LOW = ",";
static const char *NADIR = "v";

int con_rep_at(int x, int y,  const char ** const rep)
{
  double d;

  if (x < 0 || y < 0 || x >= g_width || y >= g_height)
    return E_ILLEGAL_COORDINATES;

  d = grid[y * g_width + x].c_sin;

  if (d > 0.6)
    *rep = APEX;
  else if (d > 0.2)
    *rep = HIGH;
  else if (d > -0.2)
    *rep = NIL;
  else if (d > -0.6)
    *rep = LOW;
  else
    *rep = NADIR;

  return 0;
}

static const char *titles[] = {"Alpha:", "Beta:"};
int con_input_titles(int * const n, const char * const ** const o_titles)
{
  *n = 2;
  *o_titles = titles;

  return 0;
}

int con_set_val(int x, int y, const char ** vals)
{
  double a, b;

  if (x < 0 || y < 0 || x >= g_width || y >= g_height || !vals || !vals[0] ||
      !vals[1])
    return E_ILLEGAL_COORDINATES;

  if (sscanf(vals[0], " %lf ", &a) != 1 || sscanf(vals[1], " %lf ", &b) != 1 )
    return E_BAD_VALUES;

  grid[y * g_width + g_height].alpha = a;
  grid[y * g_width + g_height].beta = b;

  return 0;
}

int con_teardown(void)
{
  if (grid)
    free(grid);

  return 0;
}

const char *con_errstring(int errno)
{
  switch (errno) {
  case 0:                     return "No error";
  case E_UNIMPLEMENTED:       return "Unimplemented function";
  case E_UNINITIALIZED:       return "Uninitialized";
  case E_ILLEGAL_COORDINATES: return "Illegal coordinates";
  case E_NULLREP:             return "Null pointer passed";
  case E_MALFORMED_INPUT:     return "Malformed input file";
  case E_BAD_VALUES:          return "Non-double values";
  default:                    return "Unknown error";
  }
}
